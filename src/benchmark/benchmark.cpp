#include <random>
#include <chrono>
#include <thread>

#include <benchmark/benchmark.h>

#include <parallel_string_search/utility.hpp>
#include <parallel_string_search/generate_test_data.hpp>

#include <parallel_string_search/unsorted_single_threaded.hpp>
#include <parallel_string_search/unsorted_multi_threaded.hpp>
#include <parallel_string_search/sorted_single_threaded.hpp>

// ////////////////////////////////////////////////////////////////////////// //
// random words
template<class FN, class...Ts>
void call_usorted(FN f, benchmark::State& state, Ts&& ...ts)
{
    const std::string_view chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    std::mt19937 gen{std::random_device{}()};
    std::uniform_int_distribution<std::size_t> d_len{1, 4};

    for (auto _ : state)
    {
        state.PauseTiming(); // Stop timers. They will not count until they are resumed.
        const auto words = cvi::testdata::generate_random_strings(state.range(0), 2, 16, gen, chars);
        const std::string querry = cvi::testdata::random_string(d_len(gen), chars, gen);
        state.ResumeTiming(); // And resume timers. They are now counting again.

        benchmark::DoNotOptimize(f(querry, words, std::forward<Ts>(ts)...));
    }
}

template<class FN>
void call_sorted(FN f, benchmark::State& state)
{
    const std::string_view chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    std::mt19937 gen{std::random_device{}()};
    std::uniform_int_distribution<std::size_t> d_len{1, 4};

    for (auto _ : state)
    {
        state.PauseTiming(); // Stop timers. They will not count until they are resumed.
        const auto words = cvi::testdata::generate_random_strings(state.range(0), 2, 16, gen, chars);
        const std::string querry = cvi::testdata::random_string(d_len(gen), chars, gen);
        const auto index = cvi::sorted_index(words);
        state.ResumeTiming(); // And resume timers. They are now counting again.

        benchmark::DoNotOptimize(f(querry, words, index));
    }
}


void usort_st_for(benchmark::State& state)
{
    call_usorted(&cvi::unsorted_single_threaded_for<std::size_t>, state);
}
void usort_st_sort_bin(benchmark::State& state)
{
    call_usorted(&cvi::unsorted_single_threaded_sort_and_bin_search<std::size_t>, state);
}
void sort_bin(benchmark::State& state)
{
    call_sorted(&cvi::sorted_single_threaded_bin_search<std::size_t>, state);
}

void usort_mt_for_blk_j1(benchmark::State& state)
{
    call_usorted(&cvi::unsorted_multi_threaded_for_blocks<std::size_t>, state, 1);
}
void usort_mt_for_blk_j2(benchmark::State& state)
{
    call_usorted(&cvi::unsorted_multi_threaded_for_blocks<std::size_t>, state, 2);
}
void usort_mt_for_blk_j3(benchmark::State& state)
{
    call_usorted(&cvi::unsorted_multi_threaded_for_blocks<std::size_t>, state, 3);
}
void usort_mt_for_blk_j4(benchmark::State& state)
{
    call_usorted(&cvi::unsorted_multi_threaded_for_blocks<std::size_t>, state, 4);
}
void usort_mt_for_blk_j5(benchmark::State& state)
{
    call_usorted(&cvi::unsorted_multi_threaded_for_blocks<std::size_t>, state, 5);
}
void usort_mt_for_blk_j6(benchmark::State& state)
{
    call_usorted(&cvi::unsorted_multi_threaded_for_blocks<std::size_t>, state, 6);
}
void usort_mt_for_blk_j7(benchmark::State& state)
{
    call_usorted(&cvi::unsorted_multi_threaded_for_blocks<std::size_t>, state, 7);
}
void usort_mt_for_blk_j8(benchmark::State& state)
{
    call_usorted(&cvi::unsorted_multi_threaded_for_blocks<std::size_t>, state, 8);
}

void usort_mt_for_shr_j1(benchmark::State& state)
{
    call_usorted(&cvi::unsorted_multi_threaded_for_shared<std::size_t>, state, 1);
}
void usort_mt_for_shr_j2(benchmark::State& state)
{
    call_usorted(&cvi::unsorted_multi_threaded_for_shared<std::size_t>, state, 2);
}
void usort_mt_for_shr_j3(benchmark::State& state)
{
    call_usorted(&cvi::unsorted_multi_threaded_for_shared<std::size_t>, state, 3);
}
void usort_mt_for_shr_j4(benchmark::State& state)
{
    call_usorted(&cvi::unsorted_multi_threaded_for_shared<std::size_t>, state, 4);
}
void usort_mt_for_shr_j5(benchmark::State& state)
{
    call_usorted(&cvi::unsorted_multi_threaded_for_shared<std::size_t>, state, 5);
}
void usort_mt_for_shr_j6(benchmark::State& state)
{
    call_usorted(&cvi::unsorted_multi_threaded_for_shared<std::size_t>, state, 6);
}
void usort_mt_for_shr_j7(benchmark::State& state)
{
    call_usorted(&cvi::unsorted_multi_threaded_for_shared<std::size_t>, state, 7);
}
void usort_mt_for_shr_j8(benchmark::State& state)
{
    call_usorted(&cvi::unsorted_multi_threaded_for_shared<std::size_t>, state, 8);
}

// ////////////////////////////////////////////////////////////////////////// //
// word list

template<class FN, class...Ts>
void call_usorted_fixed_list(FN f, benchmark::State& state, Ts&& ...ts)
{
    const std::string_view chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    std::mt19937 gen{std::random_device{}()};
    std::uniform_int_distribution<std::size_t> d_len{1, 4};
    auto words = cvi::testdata::generate_strings_of_length(4, chars);

    for (auto _ : state)
    {
        state.PauseTiming(); // Stop timers. They will not count until they are resumed.
        std::shuffle(words.begin(), words.end(), gen);
        const std::string querry = cvi::testdata::random_string(d_len(gen), chars, gen);
        state.ResumeTiming(); // And resume timers. They are now counting again.

        benchmark::DoNotOptimize(f(querry, words, std::forward<Ts>(ts)...));
    }
}
template<class FN>
void call_sorted_fixed_list(FN f, benchmark::State& state)
{
    const std::string_view chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    std::mt19937 gen{std::random_device{}()};
    std::uniform_int_distribution<std::size_t> d_len{1, 4};
    const auto words = cvi::testdata::generate_strings_of_length(4, chars);
    const auto index = cvi::sorted_index(words);

    for (auto _ : state)
    {
        state.PauseTiming(); // Stop timers. They will not count until they are resumed.
        const std::string querry = cvi::testdata::random_string(d_len(gen), chars, gen);
        state.ResumeTiming(); // And resume timers. They are now counting again.

        benchmark::DoNotOptimize(f(querry, words, index));
    }
}

void fixed_usort_st_for(benchmark::State& state)
{
    call_usorted_fixed_list(&cvi::unsorted_single_threaded_for<std::size_t>, state);
}
void fixed_usort_st_sort_bin(benchmark::State& state)
{
    call_usorted_fixed_list(&cvi::unsorted_single_threaded_sort_and_bin_search<std::size_t>, state);
}
void fixed_sort_bin(benchmark::State& state)
{
    call_sorted_fixed_list(&cvi::sorted_single_threaded_bin_search<std::size_t>, state);
}

void fixed_usort_mt_for_blk_j1(benchmark::State& state)
{
    call_usorted_fixed_list(&cvi::unsorted_multi_threaded_for_blocks<std::size_t>, state, 1);
}
void fixed_usort_mt_for_blk_j2(benchmark::State& state)
{
    call_usorted_fixed_list(&cvi::unsorted_multi_threaded_for_blocks<std::size_t>, state, 2);
}
void fixed_usort_mt_for_blk_j3(benchmark::State& state)
{
    call_usorted_fixed_list(&cvi::unsorted_multi_threaded_for_blocks<std::size_t>, state, 3);
}
void fixed_usort_mt_for_blk_j4(benchmark::State& state)
{
    call_usorted_fixed_list(&cvi::unsorted_multi_threaded_for_blocks<std::size_t>, state, 4);
}
void fixed_usort_mt_for_blk_j5(benchmark::State& state)
{
    call_usorted_fixed_list(&cvi::unsorted_multi_threaded_for_blocks<std::size_t>, state, 5);
}
void fixed_usort_mt_for_blk_j6(benchmark::State& state)
{
    call_usorted_fixed_list(&cvi::unsorted_multi_threaded_for_blocks<std::size_t>, state, 6);
}
void fixed_usort_mt_for_blk_j7(benchmark::State& state)
{
    call_usorted_fixed_list(&cvi::unsorted_multi_threaded_for_blocks<std::size_t>, state, 7);
}
void fixed_usort_mt_for_blk_j8(benchmark::State& state)
{
    call_usorted_fixed_list(&cvi::unsorted_multi_threaded_for_blocks<std::size_t>, state, 8);
}

void fixed_usort_mt_for_shr_j1(benchmark::State& state)
{
    call_usorted_fixed_list(&cvi::unsorted_multi_threaded_for_shared<std::size_t>, state, 1);
}
void fixed_usort_mt_for_shr_j2(benchmark::State& state)
{
    call_usorted_fixed_list(&cvi::unsorted_multi_threaded_for_shared<std::size_t>, state, 2);
}
void fixed_usort_mt_for_shr_j3(benchmark::State& state)
{
    call_usorted_fixed_list(&cvi::unsorted_multi_threaded_for_shared<std::size_t>, state, 3);
}
void fixed_usort_mt_for_shr_j4(benchmark::State& state)
{
    call_usorted_fixed_list(&cvi::unsorted_multi_threaded_for_shared<std::size_t>, state, 4);
}
void fixed_usort_mt_for_shr_j5(benchmark::State& state)
{
    call_usorted_fixed_list(&cvi::unsorted_multi_threaded_for_shared<std::size_t>, state, 5);
}
void fixed_usort_mt_for_shr_j6(benchmark::State& state)
{
    call_usorted_fixed_list(&cvi::unsorted_multi_threaded_for_shared<std::size_t>, state, 6);
}
void fixed_usort_mt_for_shr_j7(benchmark::State& state)
{
    call_usorted_fixed_list(&cvi::unsorted_multi_threaded_for_shared<std::size_t>, state, 7);
}
void fixed_usort_mt_for_shr_j8(benchmark::State& state)
{
    call_usorted_fixed_list(&cvi::unsorted_multi_threaded_for_shared<std::size_t>, state, 8);
}
// ////////////////////////////////////////////////////////////////////////// //
static constexpr auto n_min = 1ul;
static constexpr auto n_max = 1ul << 20;

#define call_benchmark_rng_list(f) BENCHMARK(f)->RangeMultiplier(2)->Range(n_min, n_max)->MeasureProcessCPUTime()->UseRealTime()
#define call_benchmark_fixed_list(f) BENCHMARK(f)->MeasureProcessCPUTime()->UseRealTime()

call_benchmark_rng_list(usort_st_for);
call_benchmark_rng_list(sort_bin);

call_benchmark_rng_list(usort_mt_for_blk_j1);
call_benchmark_rng_list(usort_mt_for_blk_j2);
call_benchmark_rng_list(usort_mt_for_blk_j3);
call_benchmark_rng_list(usort_mt_for_blk_j4);
call_benchmark_rng_list(usort_mt_for_blk_j5);
call_benchmark_rng_list(usort_mt_for_blk_j6);
call_benchmark_rng_list(usort_mt_for_blk_j7);
call_benchmark_rng_list(usort_mt_for_blk_j8);

call_benchmark_rng_list(usort_mt_for_shr_j1);
call_benchmark_rng_list(usort_mt_for_shr_j2);
call_benchmark_rng_list(usort_mt_for_shr_j3);
call_benchmark_rng_list(usort_mt_for_shr_j4);
call_benchmark_rng_list(usort_mt_for_shr_j5);
call_benchmark_rng_list(usort_mt_for_shr_j6);
call_benchmark_rng_list(usort_mt_for_shr_j7);
call_benchmark_rng_list(usort_mt_for_shr_j8);


call_benchmark_fixed_list(fixed_usort_st_for);
call_benchmark_fixed_list(fixed_sort_bin);

call_benchmark_fixed_list(fixed_usort_mt_for_blk_j1);
call_benchmark_fixed_list(fixed_usort_mt_for_blk_j2);
call_benchmark_fixed_list(fixed_usort_mt_for_blk_j3);
call_benchmark_fixed_list(fixed_usort_mt_for_blk_j4);
call_benchmark_fixed_list(fixed_usort_mt_for_blk_j5);
call_benchmark_fixed_list(fixed_usort_mt_for_blk_j6);
call_benchmark_fixed_list(fixed_usort_mt_for_blk_j7);
call_benchmark_fixed_list(fixed_usort_mt_for_blk_j8);

call_benchmark_fixed_list(fixed_usort_mt_for_shr_j1);
call_benchmark_fixed_list(fixed_usort_mt_for_shr_j2);
call_benchmark_fixed_list(fixed_usort_mt_for_shr_j3);
call_benchmark_fixed_list(fixed_usort_mt_for_shr_j4);
call_benchmark_fixed_list(fixed_usort_mt_for_shr_j5);
call_benchmark_fixed_list(fixed_usort_mt_for_shr_j6);
call_benchmark_fixed_list(fixed_usort_mt_for_shr_j7);
call_benchmark_fixed_list(fixed_usort_mt_for_shr_j8);
BENCHMARK_MAIN();
