#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <parallel_string_search/unsorted_single_threaded.hpp>
#include <parallel_string_search/unsorted_multi_threaded.hpp>
#include <parallel_string_search/generate_test_data.hpp>


TEST_CASE("unsorted_single_threaded_for, test single char querry")
{
    const std::vector<std::string> words   {"A", "a", "", "aW", "A", "AA", "BA", "A"};
    const std::vector<std::size_t> expected{0, 4, 5, 7};
    const std::string querry = "A";
    const auto result = cvi::unsorted_single_threaded_for<std::size_t>(querry, words);
    REQUIRE(expected == result);
    const auto result_sv = cvi::unsorted_single_threaded_for<std::string_view>(querry, words);
    const auto result_s = cvi::unsorted_single_threaded_for<std::string>(querry, words);
    REQUIRE(expected.size() == result_sv.size());
    REQUIRE(expected.size() == result_s.size());
    for (std::size_t i = 0; i < expected.size(); ++i)
    {
        REQUIRE(words.at(expected.at(i)) == result_sv.at(i));
        REQUIRE(words.at(expected.at(i)) == result_s.at(i));
    }
}

TEST_CASE("unsorted_single_threaded_for, test multi char querry")
{
    const std::vector<std::string> words   {"AA", "a", "", "aW", "AA", "AAA", "BA", "AAD"};
    const std::vector<std::size_t> expected{0, 4, 5, 7};
    const std::string querry = "AA";
    const auto result = cvi::unsorted_single_threaded_for<std::size_t>(querry, words);
    REQUIRE(expected == result);
}

TEST_CASE("unsorted_single_threaded_for, test multi char querry - 2")
{
    const std::vector<std::string> words   {"AA", "a", "", "aW", "AA", "AAA", "BA", "AAD"};
    const std::vector<std::size_t> expected{};
    const std::string querry = "AAAA";
    const auto result = cvi::unsorted_single_threaded_for<std::size_t>(querry, words);
    REQUIRE(expected == result);
}

TEST_CASE("all")
{
    const std::string_view chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    std::mt19937 gen{std::random_device{}()};
    std::uniform_int_distribution<std::size_t> d_len{1, 3};

    for (std::size_t i = 0; i < 100; ++i)
    {
        const std::string querry = cvi::testdata::random_string(d_len(gen), chars, gen);
        const auto words = cvi::testdata::generate_random_strings(100, 4, 32, chars);
        const auto to_set = [](const auto & v)
        {
            return std::set(v.begin(), v.end());
        };
        const auto expected = to_set(cvi::unsorted_single_threaded_for<std::size_t>(querry, words));
        const auto expected_s = to_set(cvi::unsorted_single_threaded_for<std::string>(querry, words));
        const auto expected_sv = to_set(cvi::unsorted_single_threaded_for<std::string_view>(querry, words));
        const auto check = [&](auto vec)
        {
            const auto set = to_set(vec);
            if (expected != set)
            {
                //debug output
                std::cout << "querry " << querry << '\n';
                std::cout << "words:\n";
                for (const auto& w : words)
                {
                    std::cout << "    " << w << '\n';
                }
                std::cout << "expected:  ";
                for (const auto& i : expected)
                {
                    std::cout << i << "  ";
                }
                std::cout << '\n';
                std::cout << "result  :";
                for (const auto& i : set)
                {
                    std::cout << "  " << i;
                }
                std::cout << '\n';
            }
            REQUIRE(expected == set);
        };
        check(cvi::unsorted_single_threaded_sort_and_bin_search<std::size_t>(querry, words));
        check(cvi::unsorted_multi_threaded_for_blocks<std::size_t>(querry, words));
        check(cvi::unsorted_multi_threaded_for_shared<std::size_t>(querry, words));

        REQUIRE(expected_s == to_set(cvi::unsorted_single_threaded_sort_and_bin_search<std::string>(querry, words)));
        REQUIRE(expected_s == to_set(cvi::unsorted_multi_threaded_for_blocks<std::string>(querry, words)));
        REQUIRE(expected_s == to_set(cvi::unsorted_multi_threaded_for_shared<std::string>(querry, words)));

        REQUIRE(expected_sv == to_set(cvi::unsorted_single_threaded_sort_and_bin_search<std::string_view>(querry, words)));
        REQUIRE(expected_sv == to_set(cvi::unsorted_multi_threaded_for_blocks<std::string_view>(querry, words)));
        REQUIRE(expected_sv == to_set(cvi::unsorted_multi_threaded_for_shared<std::string_view>(querry, words)));
    }
}
