#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <parallel_string_search/utility.hpp>


TEST_CASE("starts_with")
{
    REQUIRE(cvi::starts_with("A", "A"));
    REQUIRE(cvi::starts_with("AB", "A"));
    REQUIRE(cvi::starts_with("ABC", "AB"));
    REQUIRE(!cvi::starts_with("B", "A"));
    REQUIRE(!cvi::starts_with("BAA", "A"));
    REQUIRE(!cvi::starts_with("A", "a"));
}

