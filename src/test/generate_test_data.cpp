#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include <parallel_string_search/generate_test_data.hpp>

template<class T>
void test(const T& data, std::size_t n, std::size_t min_len, std::size_t max_len)
{
    REQUIRE(data.size() == n);
    for (const auto& s : data)
    {
        REQUIRE(s.size() >= min_len);
        REQUIRE(s.size() <= max_len);
        for (const auto c : s)
        {
            REQUIRE(((c == 'A') || (c == 'B') || (c == 'C') || (c == 'D')));
        }
    }
}

TEST_CASE("generate_random_strings, test 1")
{
    test(cvi::testdata::generate_random_strings(100, 1, 2, "ABCD"), 100, 1, 2);
}

TEST_CASE("generate_random_strings, test 2")
{
    test(cvi::testdata::generate_random_strings(10, 10, 10, "ABCD"), 10, 10, 10);
}

TEST_CASE("generate_strings_of_length")
{
    const auto gen_and_test = [](std::size_t len, std::string_view chars)
    {
        const auto data = cvi::testdata::generate_strings_of_length(len, chars);
        std::set set(data.begin(), data.end());
        REQUIRE(set.size() == std::pow(chars.size(), len));
        test(data, set.size(), len, len);
    };
    gen_and_test(2, "ABC");
    gen_and_test(3, "ABCD");
}
