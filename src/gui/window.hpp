#pragma once

#include <random>

#include <QMainWindow>
#include <QTreeWidgetItem>

namespace Ui
{
    class window;
}

class window : public QMainWindow
{
    Q_OBJECT

public:
    explicit window(QWidget* parent = nullptr);
    ~window();

private slots:

    void on_pushButtonShuffle_clicked();

    void update_entry(bool visible, unsigned long index);

    void on_pushButtonGenFixed_clicked();

    void on_pushButtonGenRng_clicked();

    void do_search(const QString& arg1);
    void on_lineEditSearch_returnPressed();
    void on_lineEditSearch_textEdited(const QString& arg1);

private:
    void clear_and_add_entries();

private:
    Ui::window* ui;

    std::vector<std::string> words;

    struct entry
    {
        bool visible = true;
        QTreeWidgetItem* item;
    };
    std::vector<entry> entries;
    std::mt19937 gen;
};

