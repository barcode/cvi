set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

add_executable(gui
    window.ui
    window.hpp
    window.cpp
    main.cpp
)
target_link_libraries(gui Qt5::Widgets parallel_string_search)





