#include <iostream>
#include <sstream>

#include <QMetaObject>

#include <parallel_string_search/unsorted_multi_threaded.hpp>
#include <parallel_string_search/generate_test_data.hpp>

#include "window.hpp"
#include "ui_window.h"

window::window(QWidget* parent) :
    QMainWindow(parent),
    ui(new Ui::window),
    gen{std::random_device{}()}
{
    ui->setupUi(this);
    std::cout << "creating AAAA-ZZZZ...\n";
    words = cvi::testdata::generate_strings_of_length(4);
    std::cout << "creating AAAA-ZZZZ...done\n";
    on_pushButtonShuffle_clicked();
}

window::~window()
{
    delete ui;
}

void window::on_pushButtonShuffle_clicked()
{
    std::cout << "shuffling words...\n";
    std::shuffle(words.begin(), words.end(), gen);
    std::cout << "shuffling words...done!\n";
    clear_and_add_entries();
}

void window::update_entry(bool visible, unsigned long index)
{
    auto& e = entries.at(index);
    if (visible != e.visible)
    {
        e.visible = visible;
        e.item->setHidden(!visible);
    }
}

void window::clear_and_add_entries()
{
    std::cout << "updating gui...\n";
    std::cout << "    removing entries...\n";
    ui->treeWidgetSearchResults->clear();
    entries.clear();
    std::cout << "    removing entries...done!\n";
    entries.reserve(words.size());
    std::cout << "    adding entries...\n";
    for (const auto& w : words)
    {
        QTreeWidgetItem* item = new QTreeWidgetItem(
            ui->treeWidgetSearchResults,
            QStringList(QString::fromStdString(w)));
        entries.emplace_back(entry{true, item});
        if (entries.size() % 10000 == 0)
        {
            std::cout << "        " << entries.size() << " / " << words.size() << '\n';
        }
    }
    std::cout << "    adding entries...done!\n";
    ui->labelNumWords->setText(QString::number(words.size()));
    do_search(ui->lineEditSearch->text());
    std::cout << "updating gui...done!\n";
}

void window::on_pushButtonGenFixed_clicked()
{
    std::cout << "creating AAAA-ZZZZ...\n";
    words = cvi::testdata::generate_strings_of_length(4);
    std::cout << "creating AAAA-ZZZZ...done\n";
    clear_and_add_entries();
}

void window::on_pushButtonGenRng_clicked()
{
    std::cout << "creating random words...\n";
    const std::string_view chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const auto [min, max] = std::minmax({ui->spinBoxGenRngMin->value(),
                                         ui->spinBoxGenRngMax->value()});
    ui->spinBoxGenRngMin->setValue(min);
    ui->spinBoxGenRngMax->setValue(max);
    const auto num = ui->spinBoxGenRngNum->value();
    std::cout << "    " << num << " words between "
              << min << " and " << max << " chars\n";
    words = cvi::testdata::generate_random_strings(num, min, max, gen, chars);
    std::cout << "creating random words...done\n";
    clear_and_add_entries();
}

void window::do_search(const QString& arg1)
{
    const auto querry = arg1.toStdString();
    if (querry.empty())
    {
        for (auto& e : entries)
        {
            if (!e.visible)
            {
                e.visible = true;
                e.item->setHidden(false);
            }
        }
        ui->labelNumberOfHits->setText(QString::number(words.size()));
        return;
    }
    std::atomic_size_t num_visible = 0;
    const auto callback = [&](bool match,
                              std::size_t index,
                              const std::string_view& /*word*/,
                              std::size_t /*tid*/)
    {
        if (match != entries.at(index).visible)
        {
            QMetaObject::invokeMethod(this, "update_entry",
                                      Qt::QueuedConnection,
                                      Q_ARG(bool, match),
                                      Q_ARG(unsigned long, static_cast<unsigned long>(index)));
        }
        num_visible += match ? 1 : 0;
    };
    using clock_t = std::chrono::high_resolution_clock;
    std::stringstream time_required;
    time_required << "Time: ";
    //querry to update the gui
    {
        std::cout << "sorting and updating gui...\n";
        auto start = clock_t::now();
        cvi::unsorted_multi_threaded_for_blocks(querry, words, callback, std::thread::hardware_concurrency());
        double elapsed_ms = std::chrono::duration_cast<std::chrono::nanoseconds>(clock_t::now() - start).count() / 1e6;
        std::cout << "sorting and updating gui...done! (took "
                  << elapsed_ms << " ms, " << num_visible.load() << " hits)\n";
        time_required << "search + gui update " << elapsed_ms << " ms";
    }
    //querry to measure search time without gui update
    if (ui->checkboxMeasureTimeWithoutUpdate->isChecked())
    {
        std::cout << "sorting (result as vec)...\n";
        auto start = clock_t::now();
        const auto vec = cvi::unsorted_multi_threaded_for_blocks<std::size_t>(querry, words, std::thread::hardware_concurrency());
        double elapsed_ms = std::chrono::duration_cast<std::chrono::nanoseconds>(clock_t::now() - start).count() / 1e6;
        std::cout << "sorting (result as vec)...done! (took "
                  << elapsed_ms << " ms, " << vec.size() << " hits)\n";
        time_required << " / search " << elapsed_ms << " ms";
    }
    ui->labelNumberOfHits->setText(QString::number(num_visible.load()));
    ui->labelSearchTime->setText(QString::fromStdString(time_required.str()));
}

void window::on_lineEditSearch_returnPressed()
{
    do_search(ui->lineEditSearch->text());
}
void window::on_lineEditSearch_textEdited(const QString& arg1)
{
    if (ui->checkBoxSearchWhileTyping->isChecked())
    {
        do_search(arg1);
    }
}
