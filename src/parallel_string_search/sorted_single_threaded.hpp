#pragma once

#include "utility.hpp"

namespace cvi
{
    template<class Callback, class = std::void_t<decltype(std::declval<Callback>()(true, 1, ""))>>
    void sorted_single_threaded_bin_search(
        const std::string& querry,
        const std::vector<std::string>& words,
        const std::vector<std::size_t>& index,
        Callback callback)
    {
        const auto range = std::equal_range(
                               index.begin(),
                               index.end(),
                               words.size(), //sentinel for querry
                               [&](auto l, auto r)
        {
            const auto sv = [&](auto i)
            {
                return i < words.size() ?
                       std::string_view(words.at(i).data(), std::min(words.at(i).size(), querry.size())) :
                       std::string_view(querry.data(), querry.size());
            };

            return sv(l) < sv(r);
        }
                           );
        for (auto it = index.begin(); it < index.end(); ++it)
        {
            callback(!(it < range.first || it >= range.second),
                     *it,
                     words.at(*it));
        }
    }

    template<class T>
    std::vector<T> sorted_single_threaded_bin_search(
        const std::string& querry,
        const std::vector<std::string>& words,
        const std::vector<std::size_t>& index)
    {
        std::vector<T> result;
        sorted_single_threaded_bin_search(
            querry,
            words,
            index,
            [&](bool match, [[maybe_unused]] std::size_t i, [[maybe_unused]] const std::string_view & w)
        {
            if (match)
            {
                if constexpr(std::is_same_v<T, std::string> || std::is_same_v<T, std::string_view>)
                {
                    result.emplace_back(w);
                }
                else
                {
                    result.emplace_back(i);
                }
            }
        });
        return result;
    }
}
