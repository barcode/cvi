#pragma once

#include "utility.hpp"
#include "sorted_single_threaded.hpp"

//unsorted_single_threaded_for
namespace cvi
{
    template<class Callback, class = std::void_t<decltype(std::declval<Callback>()(true, 1, ""))>>
    void unsorted_single_threaded_for(
        const std::string& querry,
        const std::vector<std::string>& words,
        Callback callback)
    {
        for (std::size_t i = 0; i < words.size(); ++i)
        {
            callback(starts_with(words.at(i), querry), i, words.at(i));
        }
    }

    template<class T>
    std::vector<T> unsorted_single_threaded_for(
        const std::string& querry,
        const std::vector<std::string>& words)
    {
        std::vector<T> result;
        unsorted_single_threaded_for(
            querry,
            words,
            [&](bool match, [[maybe_unused]] std::size_t i, [[maybe_unused]] const std::string_view & w)
        {
            if (match)
            {
                if constexpr(std::is_same_v<T, std::string> || std::is_same_v<T, std::string_view>)
                {
                    result.emplace_back(w);
                }
                else
                {
                    result.emplace_back(i);
                }
            }
        });
        return result;
    }
}

//unsorted_single_threaded_sort_and_bin_search
namespace cvi
{
    template<class Callback, class = std::void_t<decltype(std::declval<Callback>()(true, 1, ""))>>
    void unsorted_single_threaded_sort_and_bin_search(
        const std::string& querry,
        const std::vector<std::string>& words,
        Callback callback)
    {
        sorted_single_threaded_bin_search(
            querry,
            words,
            sorted_index(words),
            callback);
    }

    template<class T>
    std::vector<T> unsorted_single_threaded_sort_and_bin_search(
        const std::string& querry,
        const std::vector<std::string>& words)
    {
        std::vector<T> result;
        unsorted_single_threaded_sort_and_bin_search(
            querry,
            words,
            [&](bool match, [[maybe_unused]] std::size_t i, [[maybe_unused]] const std::string_view & w)
        {
            if (match)
            {
                if constexpr(std::is_same_v<T, std::string> || std::is_same_v<T, std::string_view>)
                {
                    result.emplace_back(w);
                }
                else
                {
                    result.emplace_back(i);
                }
            }
        });
        return result;
    }
}
