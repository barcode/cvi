#pragma once

#include <vector>
#include <string>
#include <string_view>
#include <random>

namespace cvi::testdata
{
    std::vector<std::string> generate_random_strings(
        std::size_t number,
        std::size_t min_length,
        std::size_t max_length,
        std::mt19937& gen,
        const std::string_view chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    );
    std::vector<std::string> generate_random_strings(
        std::size_t number,
        std::size_t min_length,
        std::size_t max_length,
        const std::string_view chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    );

    std::vector<std::string> generate_strings_of_length(
        std::size_t length,
        const std::string_view chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    );

    void random_string(std::string& str, const std::string_view chars, std::mt19937& gen);
    std::string random_string(std::size_t len, const std::string_view chars, std::mt19937& gen);
}
