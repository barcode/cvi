#pragma once

#include <vector>
#include <string>
#include <string_view>
#include <numeric>
#include <algorithm>

namespace cvi
{
    bool starts_with(const std::string& str, const std::string& querry)
    {
        return querry.size() <= str.size() &&
               querry == std::string_view(str.data(), querry.size());
    }

    std::vector<std::size_t> sorted_index(const std::vector<std::string>& v)
    {
        std::vector<std::size_t> idx(v.size());
        std::iota(idx.begin(), idx.end(), 0);
        std::sort(
            idx.begin(),
            idx.end(),
            [&](auto l, auto r)
        {
            return v.at(l) < v.at(r);
        }
        );
        return idx;
    }
}
