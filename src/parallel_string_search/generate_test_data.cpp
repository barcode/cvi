#include <stdexcept>

#include "generate_test_data.hpp"

namespace cvi::testdata
{
    void random_string(std::string& str, const std::string_view chars, std::mt19937& gen)
    {
        std::uniform_int_distribution<std::size_t> d_char{0, chars.size() - 1};
        for (auto& c : str)
        {
            c = chars.at(d_char(gen));
        }
    }
    std::string random_string(std::size_t len, const std::string_view chars, std::mt19937& gen)
    {
        std::string str(len, '\0');
        random_string(str, chars, gen);
        return str;
    }

    std::vector<std::string> generate_random_strings(
        std::size_t number,
        std::size_t min_length,
        std::size_t max_length,
        const std::string_view chars)
    {
        std::mt19937 gen{std::random_device{}()};
        return generate_random_strings(number, min_length, max_length, gen, chars);
    }
    std::vector<std::string> generate_random_strings(
        std::size_t number,
        std::size_t min_length,
        std::size_t max_length,
        std::mt19937& gen,
        const std::string_view chars)
    {
        if (chars.empty())
        {
            throw std::invalid_argument{"chars is empty!"};
        }
        if (max_length < min_length)
        {
            throw std::invalid_argument
            {
                "min_length must be smaller than max_length!"
            };
        }

        std::vector<std::string> r;
        r.reserve(number);
        std::uniform_int_distribution<std::size_t> d_length{min_length, max_length};
        while (number--)
        {
            auto l = d_length(gen);
            r.emplace_back(l, '\0');
            random_string(r.back(), chars, gen);
        }
        return r;
    }
    std::vector<std::string> generate_strings_of_length(
        std::size_t length,
        const std::string_view chars
    )
    {
        if (chars.empty())
        {
            throw std::invalid_argument{"chars is empty!"};
        }
        if (std::numeric_limits<std::size_t>::max() / chars.size() < length)
        {
            throw std::invalid_argument
            {
                "The number of generated strings would exceed std::size_t. "
                "Either reduce length or the number of chars."
            };
        }
        std::vector<std::string> r;
        r.resize(static_cast<std::size_t>(std::pow(chars.size(), length)), std::string(length, '\0'));
        for (std::size_t i = 0; i < r.size(); ++i)
        {
            auto& str = r.at(i);
            std::size_t remainder = i;
            for (std::size_t j = 0; j < length; ++j)
            {
                str.at(length - 1 - j) = chars.at(remainder % chars.size());
                remainder = remainder / chars.size();
            }
        }
        return r;
    }
}
