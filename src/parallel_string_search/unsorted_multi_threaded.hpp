#pragma once

#include <atomic>
#include <thread>

#include "utility.hpp"
#include "unsorted_single_threaded.hpp"

//unsorted_multi_threaded_for_blocks
namespace cvi
{
    template<class Callback, class = std::void_t<decltype(std::declval<Callback>()(true, 1, "", 1))>>
    std::size_t unsorted_multi_threaded_for_blocks(
        const std::string& querry,
        const std::vector<std::string>& words,
        Callback callback,
        unsigned int num_threads = std::thread::hardware_concurrency())
    {
        if (words.size() < 1 || num_threads == 1)
        {
            const auto callback2 = [&](bool match, std::size_t idx, const std::string_view & word)
            {
                callback(match, idx, word, 0);
            };
            unsorted_single_threaded_for(querry, words, callback2);
            return 1;
        }
        const auto per_t = words.size() / num_threads;
        const auto mod   = words.size() % num_threads;
        std::vector<std::thread> threads(num_threads);
        for (std::size_t tid = 0; tid < num_threads; ++tid)
        {
            threads.at(tid) = std::thread
            {
                [tid, per_t, mod, &callback, &querry, &words]
                {
                    const std::size_t offset = per_t * tid + std::min(tid, mod);
                    const std::size_t last = offset + per_t + (tid < mod ? 1 : 0);
                    for (std::size_t i = offset; i < last; ++i)
                    {
                        callback(starts_with(words.at(i), querry), i, words.at(i), tid);
                    }
                }
            };
        }
        for (auto& t : threads)
        {
            t.join();
        }
        return num_threads;
    }

    template<class T>
    std::vector<T> unsorted_multi_threaded_for_blocks(
        const std::string& querry,
        const std::vector<std::string>& words,
        unsigned int num_threads = std::thread::hardware_concurrency())
    {
        std::vector<std::vector<T>> buffer(num_threads);
        const auto callback = [&](bool match,
                                  [[maybe_unused]] std::size_t i,
                                  [[maybe_unused]] const std::string_view & w,
                                  [[maybe_unused]] std::size_t tid)
        {
            if (match)
            {
                if constexpr(std::is_same_v<T, std::string> || std::is_same_v<T, std::string_view>)
                {
                    buffer.at(tid).emplace_back(w);
                }
                else
                {
                    buffer.at(tid).emplace_back(i);
                }
            }
        };

        const auto used_num_threads = unsorted_multi_threaded_for_blocks(querry, words, callback, num_threads);
        if (used_num_threads <= 1)
        {
            return buffer.at(0);
        }
        std::vector<T> result;
        for (auto& b : buffer)
        {
            std::move(b.begin(), b.end(), std::back_inserter(result));
        }
        return result;
    }
}

//unsorted_multi_threaded_for_shared
namespace cvi
{
    template<class Callback, class = std::void_t<decltype(std::declval<Callback>()(true, 1, "", 1))>>
    std::size_t unsorted_multi_threaded_for_shared(
        const std::string& querry,
        const std::vector<std::string>& words,
        Callback callback,
        unsigned int num_threads = std::thread::hardware_concurrency())
    {
        if (words.size() < 1 || num_threads == 1)
        {
            const auto callback2 = [&](bool match, std::size_t idx, const std::string_view & word)
            {
                callback(match, idx, word, 0);
            };
            unsorted_single_threaded_for(querry, words, callback2);
            return 1;
        }
        std::atomic_size_t current_element = 0;
        std::vector<std::thread> threads(num_threads);
        for (std::size_t tid = 0; tid < num_threads; ++tid)
        {
            threads.at(tid) = std::thread
            {
                [tid, &current_element, &callback, &querry, &words]
                {
                    for (std::size_t i = current_element++; i < words.size(); i = current_element++)
                    {
                        callback(starts_with(words.at(i), querry), i, words.at(i), tid);
                    }
                }
            };
        }
        for (auto& t : threads)
        {
            t.join();
        }
        return num_threads;
    }

    template<class T>
    std::vector<T> unsorted_multi_threaded_for_shared(
        const std::string& querry,
        const std::vector<std::string>& words,
        unsigned int num_threads = std::thread::hardware_concurrency())
    {
        std::vector<std::vector<T>> buffer(num_threads);
        const auto callback = [&](bool match,
                                  [[maybe_unused]] std::size_t i,
                                  [[maybe_unused]] const std::string_view & w,
                                  [[maybe_unused]] std::size_t tid)
        {
            if (match)
            {
                if constexpr(std::is_same_v<T, std::string> || std::is_same_v<T, std::string_view>)
                {
                    buffer.at(tid).emplace_back(w);
                }
                else
                {
                    buffer.at(tid).emplace_back(i);
                }
            }
        };

        const auto used_num_threads = unsorted_multi_threaded_for_shared(querry, words, callback, num_threads);
        if (used_num_threads <= 1)
        {
            return buffer.at(0);
        }
        std::vector<T> result;
        for (auto& b : buffer)
        {
            std::move(b.begin(), b.end(), std::back_inserter(result));
        }
        return result;
    }
}
